import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
const App = () => {
  const [kategori, setKategori] = useState([
    {
      nama: 'Ice',
    },
    {
      nama: 'Boba',
    },
    {
      nama: 'Alkohol',
    },
    {
      nama: 'Salad',
    },
    {
      nama: 'Air Mineral',
    },
    {
      nama: 'Minuman',
    },
    {
      nama: 'Jamu',
    },
  ]);

  const [kategoriSeleksi, setKategoriSeleksi] = useState({
    nama: 'Ice',
  });

  const [dataTrending, setDataTrending] = useState([
    {
      namaResep: 'Ice Bland',
      author: 'Anggi',
      image: require('./src/images/icebland.jpg'),
    },
    {
      namaResep: 'Ice Anggur Alkohol',
      author: 'Nanda',
      image: require('./src/images/iceanggur.jpg'),
    },
    {
      namaResep: 'Boba Spesial 3 Rasa',
      author: 'Fernandes',
      image: require('./src/images/Boba-9.jpg'),
    },
    {
      namaResep: 'Strowbery Senja',
      author: 'Made',
      image: require('./src/images/icered.jpg'),
    },
  ]);

  const [dataVideo, setDataVideo] = useState([
    {
      namaResep: 'Boba Spesial 3 Rasa',
      author: 'Anggi',
      image: require('./src/images/Boba-9.jpg'),
      length: '10:52',
    },
    {
      namaResep: 'Strowbery Senja',
      author: 'Nanda',
      image: require('./src/images/icered.jpg'),
      length: '12:02',
    },
    {
      namaResep: 'Ice Bland',
      author: 'Fernandes',
      image: require('./src/images/icebland.jpg'),
      length: '12:13',
    },
    {
      namaResep: 'Ice Anggur Alkohol',
      author: 'Made',
      image: require('./src/images/iceanggur.jpg'),
      length: '05:14',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#f5f5f5'}}>
      <ScrollView>
        <StatusBar backgroundColor="#f5f5f5" barStyle="dark-content" />
        <View style={{marginHorizontal: 120, marginBottom: 20, marginTop: 50}}>
          <Text style={{fontSize: 25, fontWeight: 'bold', color: '#00FFFF'}}>
            Dahaga<Text style={{color: '#FFA500'}}>KU</Text>
          </Text>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor:
                    kategoriSeleksi.nama == item.nama ? '#6B8E23' : '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}
                onPress={() => setKategoriSeleksi(item)}>
                <Text
                  style={{
                    color:
                      kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
                  }}>
                  {item.nama}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#800000'}}>
              Trending
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataTrending}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <Image
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}
                />
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#8B0000'}}>
              Video Tutorial
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataVideo}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <ImageBackground
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}>
                  <View style={{flex: 1}}>
                    <View style={{flex: 1}}></View>
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        <Icon
                          style={{marginLeft: 5}}
                          name="play-circle"
                          size={15}
                          color="#bdbdbd"
                        />
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingRight: 10,
                          paddingTop: 2,
                          paddingBottom: 2,
                          paddingLeft: 4,
                        }}>
                        <Text style={{color: '#FFFFFF'}}>{item.length}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          paddingTop: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="home" size={25} color="#7FFF00" />
          <Text style={{color: '#006400'}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="search" size={25} color="#bdbdbd" />
          <Text style={{color: '#bdbdbd'}}>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="file-tray-full" size={25} color="#bdbdbd" />
          <Text style={{color: '#bdbdbd'}}>Kategori</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="person" size={25} color="#bdbdbd" />
          <Text style={{color: '#bdbdbd'}}>User</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;
